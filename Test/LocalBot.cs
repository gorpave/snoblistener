﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Pauk.SnobSpider.Test
{
    internal class LocalBot
    {
        private readonly Mock<ITelegramBotClient> _botMock;
        public readonly List<string> ReceivedMessages = new();
        public Action<long?, string> ProcessReceiveMessage = (l, s) => { };

        public LocalBot(string ignoredUser)
        {
            _botMock = new Mock<ITelegramBotClient>();
            _botMock.Setup(x => x.MakeRequestAsync(
                It.IsAny<SendMessageRequest>(),
                    It.IsAny<CancellationToken>()))
                .Returns<ChatId, string, ParseMode, bool, bool, int, IReplyMarkup, CancellationToken>(
                    (chatId, message, x0, x1, x2, x3, x4, x5) =>
                    {
                        if (chatId != ignoredUser)
                        {
                            ReceivedMessages.Add(message);
                            ProcessReceiveMessage(chatId.Identifier, message);
                        }
                        return Task.FromResult(
                            new Message()
                        );
                    }
                );
            _botMock.Setup(x => x.GetMeAsync(It.IsAny<CancellationToken>())).Returns<CancellationToken>(x =>
                Task.FromResult(new User
                {
                    IsBot = true
                }));
        }

        public ITelegramBotClient GetClient()
        {
            return _botMock.Object;
        }
    }
}