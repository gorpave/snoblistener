﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Pauk.SnobSpider.Test
{
    public class SimpleServer
    {
        private const int ServerStartPort = 7123;
        private static int _serverCurrentPort = ServerStartPort;
        public readonly string ServerUrl;
        private HttpListener _listener;
        public Func<Uri, string> Answer = uri => "404";

        public SimpleServer()
        {
            ServerUrl = $"http://127.0.0.1:{_serverCurrentPort++}/";
        }

        public void Start()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add(ServerUrl);
            _listener.Start();
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var context = _listener.GetContext();
                    Process(context);
                }
            });
        }

        public void AddAddress(string url)
        {
            _listener.Prefixes.Add(ServerUrl + url.Remove(0, 1) + '/');
        }

        private void Process(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            var prepareAnswer = Answer(request.Url);
            var responseStr = prepareAnswer;
            var buffer = Encoding.UTF8.GetBytes(responseStr);
            response.ContentLength64 = buffer.Length;
            var output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();
        }
    }
}