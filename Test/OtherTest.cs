using System;
using System.Globalization;
using Xunit;

namespace Pauk.SnobSpider.Test
{
    public class OtherTest
    {
        [Fact]
        public void TestSupportFormatting()
        {
            var result = DateTime.TryParse("31 Декабря 2018 21:00", CultureInfo.GetCultureInfo("ru-RU"),
                DateTimeStyles.None, out _);
            Assert.True(result);
        }
    }
}