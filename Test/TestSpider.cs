

//[assembly:CollectionBehavior(DisableTestParallelization = true)]

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Xunit;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Telegram.Bot.Exceptions;
using Xunit;
using Xunit.Abstractions;

namespace Pauk.SnobSpider.Test
{
    public class TestSpider
    {
        public TestSpider(ITestOutputHelper outputHelper)
        {
            _localSnobServer = new SimpleServer();
            _adminId = _random.Next(int.MaxValue);
            var serviceCollection = new ServiceCollection();
            var channelId = 123;
            serviceCollection.Configure<TelegramBotOptions>(
                x =>
                {
                    x.ExpirationTime = TimeSpan.FromDays(1);
                    x.AdminChatId = _adminId;
                    x.ChannelId = 123;
                });
            serviceCollection.Configure<LinkGetterOption>(options =>
            {
                options.Url = new Uri(_localSnobServer.ServerUrl);
            });
            serviceCollection.AddSingleton<ITelegramBot, TelegramBot>();
     
            serviceCollection.AddTransient<CheckSiteJob>();
            serviceCollection.AddTransient<HtmlWeb>();
            
            _localBot = new LocalBot(channelId.ToString());
            serviceCollection.AddSingleton(_localBot.GetClient());
            serviceCollection.AddLogging();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            
            var botService = serviceProvider.GetRequiredService<ITelegramBot>();
           
            _localSnobServer.Start();

            _checkSiteJob = serviceProvider.GetRequiredService<CheckSiteJob>();
           
        }

        private readonly int _adminId;
        private readonly SimpleServer _localSnobServer;
        private readonly Random _random = new ();
        private readonly LocalBot _localBot;
        private readonly CheckSiteJob _checkSiteJob;

        private async Task CheckSite()
        {
            await _checkSiteJob.Execute();
        }

        private  string GenerateActivityTag(string link)
        {
            return
                $"<a href=\"{link}\">\n\t<div class=\"storage__item storage__item_cover\">\n\t\t<div class=\"storage__item__text\">\n\t\t\t<div class=\"title\">{link.GetHashCode()+ _random.Next(5000)}</p>\n\t\t</div>\n\t</div>\n</a>";
        }

        [Fact]
        public async Task SpiderMustNotifyAboutLinks()
        {
            var link = "/selected/entry/145991";
            var messageCount = 0;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
               
                messageCount++;
            };

            _localSnobServer.Answer = uri => GenerateActivityTag(link) +
                                             "<div class=\"bestOtherBlogs__item visible-d col-md-4\"><a href=\"/selected/entry/145993\"></div>" +
                                             "<div class=\"bestOtherBlogs__itemvisible-dcol-md-4\"><a href=\"\"><imgsrc=\"\"alt=\"\"></a><a href=\"\"></a><p class=\"title\"><a href=\"/entry/165173\">«Онвсемнадоел»:ИгорьСтрелковидругиеогибелиАлександраЗахарченко</a></p></div>";

            

            await CheckSite();
            Assert.Equal(1, messageCount);
        }

        [Fact]
        public async Task UniqueLinkMustAppearOnlyOneTime()
        {
            const string link = "/selected/entry/145991";

            var startSite = GenerateActivityTag("/selected/entry/145993") +
                            GenerateActivityTag(link);
            _localSnobServer.Answer = uri => startSite;
         
            var messageWas = false;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                Assert.Equal(id, _adminId);
                messageWas = true;
            };
            await CheckSite();


            Assert.True(messageWas, "First message not receive");


            _localSnobServer.Answer = uri => GenerateActivityTag("/selected/entry/145993");
            await CheckSite();


            _localSnobServer.Answer = uri => startSite;
            messageWas = false;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                Assert.Equal(id, _adminId);
                messageWas = true;
            };
            await CheckSite();

            Assert.False(messageWas);
        }
    }
}