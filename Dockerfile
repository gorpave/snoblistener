FROM mcr.microsoft.com/dotnet/sdk:8.0 as build
WORKDIR /src
COPY SnobSpider/ SnobSpider/

WORKDIR /src/SnobSpider
RUN dotnet publish Pauk.SnobSpider.csproj -c Release -o /app

FROM mcr.microsoft.com/dotnet/runtime:8.0
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "Pauk.SnobSpider.dll"]

