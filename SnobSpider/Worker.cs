using FluentScheduler;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;
using Pauk.SnobSpider.Infrastructure.Sheduler;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Polly;
using Polly.Retry;

namespace Pauk.SnobSpider;

public class Worker : BackgroundService
{
    private readonly Registry _registry;
    private readonly ITelegramBot _telegramBot;
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly IOptions<SchedulerOption> _options;
    private readonly ILogger<Worker> _logger;
    private readonly AsyncRetryPolicy _retryPolicy;

    public Worker(
        Registry registry, 
        ITelegramBot telegramBot, 
        IServiceScopeFactory scopeFactory,
        IOptions<SchedulerOption> options,
        ILogger<Worker> logger)
    {
        _registry = registry;
        _telegramBot = telegramBot;
        _scopeFactory = scopeFactory;
        _options = options;
        _logger = logger;
        _retryPolicy = Policy
            .Handle<TaskCanceledException>()
            .Or<System.Net.WebException>()
            .WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(3),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10),
                TimeSpan.FromSeconds(20)
            }, (exception, timeSpan, retryCount, context) =>
            {
                if (retryCount < 4)
                {
                    return;
                }
                _logger.LogWarning(exception, "Ошибка при обращении к " +
                                           "таблицам попытка {RetryCount} итнтервал {TimeSpan}",retryCount , timeSpan);
            });
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        JobManager.Initialize(_registry);
        await _telegramBot.Start(stoppingToken);
        
        while (!stoppingToken.IsCancellationRequested)
        {
            await using var scope = _scopeFactory.CreateAsyncScope();
            var checkSiteJob = scope.ServiceProvider.GetRequiredService<CheckSiteJob>();
            try
            {
                await _retryPolicy.ExecuteAsync(async () => await checkSiteJob.Execute());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Проблема при прогоне");
                await Task.Delay(TimeSpan.FromMinutes(1), stoppingToken);
            }
           
            await Task.Delay(_options.Value.CheckSiteInterval, stoppingToken);
        }
    }
}