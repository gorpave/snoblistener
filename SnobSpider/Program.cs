﻿using FluentScheduler;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using Pauk.SnobSpider;
using Pauk.SnobSpider.DAL;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Infrastructure.Sheduler;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Serilog;
using Serilog.Events;
using Telegram.Bot;

var configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", false, true)
    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true)
    .AddEnvironmentVariables()
    .Build();

var host = Host.CreateDefaultBuilder(args)
    .UseSerilog()
    .ConfigureServices(serviceCollection =>
    {
        serviceCollection.Configure<TelegramBotOptions>(options => configuration.GetSection("Telegram").Bind(options));
        serviceCollection.Configure<SchedulerOption>(options => configuration.GetSection("Scheduler").Bind(options));
        serviceCollection.Configure<LinkGetterOption>(options => configuration.GetSection("Snob").Bind(options));
        serviceCollection.AddTransient<ApplicationDbContext>();
        serviceCollection.AddSingleton<ITelegramBot, TelegramBot>();
        serviceCollection.AddTransient<CheckSiteJob>();
        serviceCollection.AddTransient<ISiteProccessor, SiteProcessor>();
    
        serviceCollection.AddSingleton<Registry, Sheduler>();
        serviceCollection.AddTransient<HtmlWeb>();
        serviceCollection.AddHostedService<Worker>();
        serviceCollection
            .AddDbContext<ApplicationDbContext>((_, options) =>
            {
                options.UseSqlite("Data Source=./db/snob.db;");
            });
        var token = configuration["Telegram:Token"];
        var telegramBotClient = new TelegramBotClient(token);
        serviceCollection.AddSingleton<ITelegramBotClient>(telegramBotClient);
        
        serviceCollection.AddAsyncInitializer<DbInitializer>();
    })
    .Build();

Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    .ReadFrom.Configuration(configuration)
    .WriteTo.File(
        "logs/.log",
        rollingInterval: RollingInterval.Day,
        outputTemplate: "{Timestamp:dd MMM HH:mm:ss} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
        retainedFileCountLimit: 20)
    .CreateLogger();

await host.InitAsync();
await host.RunAsync();

