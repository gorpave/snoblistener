﻿using Microsoft.EntityFrameworkCore;
using Pauk.SnobSpider.Model;

namespace Pauk.SnobSpider.DAL;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(
        DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }
        
    public DbSet<LinkActivity> LinkActivities { get; set; } = null!;
}