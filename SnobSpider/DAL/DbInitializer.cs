﻿using Extensions.Hosting.AsyncInitialization;
using Microsoft.EntityFrameworkCore;

namespace Pauk.SnobSpider.DAL;

internal class DbInitializer : IAsyncInitializer
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<DbInitializer> _logger;

    public DbInitializer(ApplicationDbContext context, ILogger<DbInitializer> logger)
    {
        _context = context;
        _logger = logger;
    }

    public async Task InitializeAsync(CancellationToken cancellationToken)
    {
        _logger.LogDebug("Старт миграции БД");
        Directory.CreateDirectory("db");
        await _context.Database.MigrateAsync(cancellationToken: cancellationToken);
        _logger.LogDebug("Успешное завершение миграции БД");
    }
}