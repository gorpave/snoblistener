﻿namespace Pauk.SnobSpider.Model;

public class LinkActivity
{
    public int Id { get; set; }
    public Uri Url { get; set; } = null!;
    public string Header { get; set; } = null!;
    public DateTime CreateDateTime { get; set; }
        
    public DateTime? ActivityDateTime { get; set; }
}