namespace Pauk.SnobSpider.Model;

public class ActivityDto
{
    public string Header { get; set; } = null!;
    public Uri Link { get; set; } = null!;
    public DateTime? ActivityDate { get;  set; }

    public override string ToString()
    {
        return $"h:{Header} l:{Link}";
    }
}