﻿using System.Globalization;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.Model;

namespace Pauk.SnobSpider.Infrastructure.LinkGetter;

internal class SiteProcessor : ISiteProccessor
{
    private readonly HtmlWeb _htmlWeb;
    private readonly ILogger<SiteProcessor> _logger;
    private readonly Uri _mainUri;

    public SiteProcessor(HtmlWeb htmlWeb, ILogger<SiteProcessor> logger, IOptions<LinkGetterOption> options)
    {
        _htmlWeb = htmlWeb;
        _logger = logger;
        _mainUri = options.Value.Url;
    }

    public IEnumerable<ActivityDto> GetCurrentShowCase()
    {
        var uri = _mainUri;
        var source = _htmlWeb.Load(uri);
        var nodes = source.QuerySelectorAll(".content-list__items-list-item");
        var links = new List<ActivityDto>();
        foreach (var htmlNode in nodes)
        {
            var title = htmlNode.QuerySelector(".text_block-title").InnerText;
            var link = new Uri(uri, htmlNode.QuerySelector("a").Attributes["href"].Value);
            var activityDate = GetDateFromSnob(htmlNode.QuerySelector("time").Attributes["datetime"].Value);
            links.Add(new ActivityDto
            {
                Link = link,
                Header = title,
                ActivityDate = activityDate
            });
        }
            
        return links;
    }

    private static DateTime? GetDateFromSnob(string activityDate)
    {
        var cleanString = activityDate.Replace("\n", "").Trim();
        var pureDate = Regex.Replace(cleanString, " {2,}", " ");
        var success = DateTime.TryParse(pureDate, CultureInfo.GetCultureInfo("ru-RU"), DateTimeStyles.None,
            out var result);
        if (success) return result;
        return null;
    }
}