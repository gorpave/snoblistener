﻿namespace Pauk.SnobSpider.Infrastructure.LinkGetter;

internal class LinkGetterOption
{
    public Uri Url { get; set; } = null!;
}