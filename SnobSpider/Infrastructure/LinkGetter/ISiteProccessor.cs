﻿using Pauk.SnobSpider.Model;

namespace Pauk.SnobSpider.Infrastructure.LinkGetter;

internal interface ISiteProccessor
{
    IEnumerable<ActivityDto> GetCurrentShowCase();
}