﻿using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.DAL;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Pauk.SnobSpider.Model;
using Telegram.Bot;

namespace Pauk.SnobSpider.Infrastructure.CheckJobStructure;

internal class CheckSiteJob
{
    private readonly ApplicationDbContext _appContext;
    private readonly ISiteProccessor _siteProcessor;
    private readonly ILogger<CheckSiteJob> _logger;
    private readonly ITelegramBotClient _telegramBot;
    private readonly long _channelId;

    public CheckSiteJob(
        ApplicationDbContext appContext,
        ITelegramBotClient telegramBot,
        ISiteProccessor siteProcessor,
        IOptions<TelegramBotOptions> telegramOptions,
        ILogger<CheckSiteJob> logger)
    {
        _appContext = appContext;

        _telegramBot = telegramBot;
        _siteProcessor = siteProcessor;
        _channelId = telegramOptions.Value.ChannelId;
        _logger = logger;
    }

    public async Task Execute()
    {
        await Check();
    }

    private async Task Check()
    {
        var activities = _siteProcessor.GetCurrentShowCase();

        var newActivities = new List<ActivityDto>();
        foreach (var activity in activities)
        {
            if (await _appContext.LinkActivities.AnyAsync(x => x.Url == activity.Link))
            {
                continue;
            }
            newActivities.Add(activity);
        }

        foreach (var activity in newActivities)
        {
            await _telegramBot.SendTextMessageAsync(_channelId,
                $"{activity.Link}\n{activity.ActivityDate?.ToString("dd MMMM", CultureInfo.GetCultureInfo("ru-RU"))}");
                
            _logger.LogInformation("Новое мероприятие {Header}", activity.Header);
                
            _appContext.LinkActivities.Add(new LinkActivity()
            {
                Url = activity.Link,
                Header = activity.Header,
                ActivityDateTime = activity.ActivityDate,
                CreateDateTime = DateTime.UtcNow
            });
            await _appContext.SaveChangesAsync();
        }
    }
}