﻿using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Pauk.SnobSpider.Infrastructure.TelegramBot;

internal class TelegramBot : ITelegramBot
{
    private readonly ITelegramBotClient _client;

    private readonly ILogger<TelegramBot> _logger;
    private readonly TelegramBotOptions _options;

    public TelegramBot(
        ITelegramBotClient client,
        ILogger<TelegramBot> logger, 
        IOptions<TelegramBotOptions> options)
    {
        _client = client;
        _logger = logger;
        _options = options.Value;
        _logger.LogInformation("channel id {ChannelId}", _options.ChannelId);
    }

    public async Task Start(CancellationToken cancellationToken)
    {
        var receiverOptions = new ReceiverOptions
        {
            AllowedUpdates = Array.Empty<UpdateType>(),// receive all update types
        };

        _client.StartReceiving(
            updateHandler: HandleUpdateAsync,
            errorHandler: HandlePollingErrorAsync,
            receiverOptions: receiverOptions,
            cancellationToken: cancellationToken
        );
            
        var botInfo = await _client.GetMeAsync(cancellationToken: cancellationToken);
   
        await _client.SendTextMessageAsync(_options.AdminChatId, $"Я {botInfo.Username} готов, Хозяин!", cancellationToken: cancellationToken);

        _logger.LogInformation("Bot {botName} start listen", botInfo.Username);
    }

    private Task HandleUpdateAsync(ITelegramBotClient client, Update update, CancellationToken cancellationToken)
    {
        // Only process Message updates: https://core.telegram.org/bots/api#message
        if (update.Type != UpdateType.Message)
            return Task.CompletedTask;
        // Only process text messages
        if (update.Message!.Type != MessageType.Text)
            return Task.CompletedTask;

        var chatId = update.Message.Chat.Id;
        var messageText = update.Message.Text;

        _logger.LogInformation($"Received a '{messageText}' message in chat {chatId}.");
            
        return Task.CompletedTask;
    }

    private Task HandlePollingErrorAsync(ITelegramBotClient client, Exception exception, CancellationToken cancellationToken)
    {
        _logger.LogWarning(exception,$"Ошибка во время работы бота");
        return Task.CompletedTask;
    }

    public async Task SendAdmin(string message)
    {
        await _client.SendTextMessageAsync(_options.AdminChatId, message);
    }
}