﻿namespace Pauk.SnobSpider.Infrastructure.TelegramBot;

internal class TelegramBotOptions
{
    public long AdminChatId { get; set; }
    public long ChannelId { get; set; }
    public TimeSpan ExpirationTime { get; set; }
}