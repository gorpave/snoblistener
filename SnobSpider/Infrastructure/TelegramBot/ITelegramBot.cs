﻿namespace Pauk.SnobSpider.Infrastructure.TelegramBot;

public interface ITelegramBot
{
    Task Start(CancellationToken cancellationToken);
    Task SendAdmin(string message);
}