﻿namespace Pauk.SnobSpider.Infrastructure.Sheduler;

public class SchedulerOption
{
    public TimeSpan CheckSiteInterval { get; set; }
}