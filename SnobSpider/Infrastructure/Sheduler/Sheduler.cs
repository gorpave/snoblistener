﻿using FluentScheduler;
using Microsoft.Extensions.Options;

namespace Pauk.SnobSpider.Infrastructure.Sheduler;

internal class Sheduler : Registry
{
    public Sheduler(ILogger<Sheduler> logger,
        IOptions<SchedulerOption> options)
    {
        var interval = options.Value.CheckSiteInterval;
        NonReentrantAsDefault();
        // Schedule(() => checkSiteJob).ToRunNow().AndEvery((int) interval.TotalMilliseconds).Milliseconds();
        // Schedule(() => checkBotJob).ToRunEvery(1).Months().OnTheLastDay().At(12,00);
        logger.LogInformation("Site will be check every {interval}", interval);
    }
}